#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <semaphore.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <vector>
#include <math.h>
#include <limits>

using namespace std;

int  atoia(char *src, int *dst)
{
    int index = 0;

    while(*src != '\n')
    {
        if(isspace(*src))
        {
            index++;
            src++;
        }
        else
        {
            dst[index] = dst[index] * 10 + *src - '0';
            src++;
        }
    }
    
    return dst[0];
}


int compare (const void * a, const void * b)
{
  return ( *(int*)a - *(int*)b );
}

double geometric_mean(std::vector<double> const & data)
{
    double m = 1.0;
    long long ex = 0;
    double invN = 1.0 / data.size();
    int index;
	
    for (index = 0; index < data.size(); index++)
    {
        int i;

	double x = data[index];

        double f1 = frexp(x,&i);
        m*=f1;
        ex+=i;
    }

    return pow( std::numeric_limits<double>::radix, ex * invN) * pow(m,invN);
}

int main()
{
    const char *name = "OS";
    const int SIZE = 4096;

    int shm_fd;
    char *ptr;
    int number;
    std::vector<double>  vec;
    
    printf("[childB] Child process started\n");

    /* open the shared memory segment */
    shm_fd = shm_open(name, O_RDWR, 0666);
    if (shm_fd == -1) {
        printf("shared memory failed\n");
        exit(-1);
    }

    /* now map the shared memory segment in the address space of the process */
    ptr = (char *)mmap(0, SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd, 0);
    if (ptr == MAP_FAILED) {
        printf("Map failed\n");
        exit(-1);
    }

    sem_t * sem = sem_open ( "mem_semaphore" , O_CREAT | O_EXCL , S_IRUSR | S_IWUSR , 0) ;

    while(1)
    {            
        int sorted_mem[100] = {0};
        
        int count = atoia(ptr, sorted_mem);

        vec.clear();
        
        sleep(1);        
       
	// printf("child B count: %d\n", count);
 
        if(count > 0)
        {           
            // sem_trywait( sem ) ;
            
            printf("[childB] Random Numbers Received From Shared Memory: ");

            for(int i = 0; i < sorted_mem[0]; i++)
            {
                printf("%d ", sorted_mem[i+1]);
                vec.push_back(sorted_mem[i+1]);
            }
            printf("\n");

            qsort (sorted_mem + 1, sorted_mem[0] , sizeof(int), compare);

            printf("[childB] Sorted sequence: ");

            for(int i = 0; i < sorted_mem[0]; i++)
            {
                printf("%d ", sorted_mem[i+1]);
            }
            printf("\n");            

            double geome_mean = geometric_mean(vec);

            printf("[childB] Geometric Mean: %lf\n", geome_mean);        
            
            *ptr = '0';
            *(ptr + 1) = ' ';
          
            count = 0; 
            //sem_post( sem );            
        }
    }

    /* remove the shared memory segment */
    if (shm_unlink(name) == -1) 
    {
        printf("Error removing %s\n",name);
        exit(-1);
    }

    return 0;
}
