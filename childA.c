
/************************************************************************/
/*   childA.c DESCRIPTION:                                       */
/*   Take the data from stdin and add the a suffix to the data          */
/*   Then write the processed data to stdout.                           */
/************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/mman.h>


int main(int argc, char * argv[])
{
    char token[100];
    char number[100];
    int i; 
    long long summation = 0;
    
    printf("[childA] child process started\n");
    
    if(argc < 2)
    {
        perror("Specify a process name please!\n");
        /* check the number of arguments */
        exit(1);
    }
    
    while(scanf("%s",token) != EOF)
    {
        /* reveice the number */
        
        summation = 0;        
        
        printf("[childA] Random Numbers Received From Pipe: ");
        
        for(i = 0; i < atoi(token); i++)
        {
            scanf("%s", number) ;
            
            summation += atoi(number);    
            
            printf("%d ", atoi(number));
        }
        
        double mean = summation / (float) atoi(token);
        
            
        printf("\n[childA] Median: %f \n", mean);
        // printf("%s--->%s-RECEIVED/SENT-TO-stdout\n", token, argv[1]);
        /* printf("%s-%s-RECEIVED/SENT to STDOUT \n",token, argv[1]);
        /* process data from stdin and write to stdout */
        fflush(stdout);	
    }
    _exit(0);
}
