#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <time.h>
#include <fcntl.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <semaphore.h>
#include <signal.h>

char *shm_base;
sem_t * sem;

int create_shm_childB(void)
{
    const int SIZE = 4096;
    const char *name = "OS";
    const char *message0= "Studying ";
    const char *message1= "Operating Systems ";
    const char *message2= "Is Fun!";

    int shm_fd;
    

    sem = sem_open ( "mem_semaphore" , O_CREAT | O_EXCL , S_IRUSR | S_IWUSR , 0) ;

    /* create the shared memory segment */
    shm_fd = shm_open(name, O_CREAT | O_RDWR, 0666);

    /* configure the size of the shared memory segment */
    ftruncate(shm_fd,SIZE);

    /* now map the shared memory segment in the address space of the process */
    shm_base = mmap(0, SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd, 0);
    if (shm_base == MAP_FAILED) {
        printf("Map failed\n");
        return -1;
    }

    *shm_base = '0';
    *(shm_base + 1) = ' ';
    
    return 0;
}

int main()
{
    char token[100];
    FILE *fout;
    int p[2];
    int pid1, pid2;
    
    pipe( p ); /* pipe between parent and child */

    if((pid1 = fork()) == 0)
    {/* child process P1 executes */
        close(p[1]); /* close the fd of write end of the pipe */
        if(dup2(p[0], 0 ) == -1 ) /* refer stdin to the read end of the pipe */
        {
            perror( "dup2 failed" );
            _exit(1);
        }
        close(p[0]); /* close the fd of read end of the pipe */
        execl("childA","childA","CHILD", 0);

        perror("execl childA failed");
        _exit(1);
    }
    else if(pid1 < 0)
    {/* fork() failed */
        perror("fork CHILD failed");
        exit(1);
    }

    /* parent executes */
    
    close(p[0]); /* close the fd of the read end of pipe */

   /* use fdopen() to associate a write stream to the write end
      of the pipe between parent and child */
    if( ( fout = fdopen( p[1], "w" ) ) == 0 )
    {
        perror( "fdopen failed" );
        exit(1);
    }

    srand(time(NULL));
    

    if((pid2 = fork()) == 0)
    {
        /* child process P2 executes */

        execl("childB","childB","CHILD", 0);

        perror("execl childB failed");
        _exit(1);
    }
    else if(pid2 < 0)
    {/* fork() failed */
        perror("fork CHILD failed");
        exit(1);
    }
    else
    {
        create_shm_childB();
    }
       
    
    while(1)
    {        
        printf("\n[mainParent] Enter a positive integer or 0 to exit\n");
        
        scanf( "%s", token );
        
        if(atoi(token) == 0)
        {
            // kill signal to kill all child process
            kill(pid1, SIGQUIT);    
            kill(pid2, SIGQUIT);                 
            
            printf("[mainParent] Process Waits\n");
            wait(NULL);
            
            printf("[mainParent] Process Exits\n");
            exit(1);
        }
        else if(atoi(token) > 0 && (atoi(token) == atof(token)))
        {
            int number = atoi(token);
            int randn = 0;
            int i; 
            char* ptr = shm_base;    

            fprintf(fout, "%d ", number );
            
            ptr += sprintf(ptr, "%d ", number);    

            printf("[mainParent] Generating %d random integers: ", number);
            
            
            for( i = 0; i < number; i++ )
            {
                randn = 50 + rand() % 50;   
            
                printf("%d ", randn);
            
                fprintf( fout, "%d ", randn );

               // sem_trywait( sem ) ;    
                
                ptr += sprintf(ptr, "%d ", randn);

               // sem_post( sem ) ;
            }                       
            
            printf("\n");
            fflush( fout );      

            // wait until child B had processed
            while(*shm_base != '0');
        }
        else
        {
            printf("[mainParent] Can only accept positive integers, please try again.\n");
        }
                
    }

    close(1); /* close reference to pipe in order to sending 'EOF' */
    
    wait(NULL);

    return(0);
}
